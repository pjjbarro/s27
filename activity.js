let http = require('http');

http.createServer(function(req,res){

	console.log(req.url)//endpoint
	console.log(req.method)//method of request


	if(req.url === "/users" && req.method === "GET"){

		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(users));

	}

	else if(req.url === "/users" && req.method === "POST"){


		let requestBody = "";

		req.on('data',function(data){


			requestBody += data;

		})

		req.on('end', function(){
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let newUser = {

				username: requestBody.username,
				password: requestBody.password

			}

			users.push(newUser);
			console.log(users);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.end("Registration Successful");


		})

	}

		else if(req.url === "/users/login" && req.method === "POST"){


		let requestBody = "";

		req.on('data',function(data){


			requestBody += data;

		})

		req.on('end', function(){
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			let foundUser = users.find((user)=>{

				console.log(user);
				//return condition for find allows us to check if the object/item currently looped by find() matches our condition. And if it did match the condition, the current object being looped will be return and we can save it in a variable.
				//If no item in the array matches the condition, find() will return undefined.
				return user.username === requestBody.username && user.password === requestBody.password;

			})

			console.log(foundUser);

			if(foundUser !== undefined){

				res.writeHead(200,{"Content-Type": "application/json"});
				res.end(JSON.stringify(foundUser));

			} else {

				res.writeHead(401,{"Content-Type": "application/json"});
				res.end("Login Failed. Wrong Credentials.");

			}



		})

	}


	else {

		res.writeHead(404,{'Content-Type': 'text/plain'});
		res.end('Resource Not Found!')

	}

}).listen(8000);

console.log(`Server is running at port 8000`)
