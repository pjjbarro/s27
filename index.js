let http = require('http');

/*

	HTTP Requests or request with HTTP servers are defined by their methods. These HTTP methods allow us to identify the action we want done per our request. These methods are primarily concerned with CRUD operations.

	Common HTTP Methods

	GET - Get method for a request indicates that we want to get or retrieve data.

	POST - Post method for request indicates that we want to add or send data to the server for processing. Post method requests are usually used for creating new entries/documents.

	PUT  - Put method for request indicates that we want to edit or update a document or entry. This method also means that we are to pass data into the server.

	DELETE - Delete method allows us to identify that the action a request wants is to delete a specified document or entry.

	We can actually the same endpoints per request but different methods.
	This is so you can actually group your endpoints and requests together.


*/

let products = [

	{
		name: "Nacho Cheese",
		price: 50,
		stocks: 10
	},
	{
		name: "Clover Chips",
		price: 40,
		stocks: 10
	},
	{
		name: "KitKat",
		price: 35,
		stocks: 20
	}

];

http.createServer(function(req,res){

	console.log(req.url)//endpoint
	console.log(req.method)//method of request

	if(req.url === "/" && req.method === "GET" ){

		res.writeHead(200,{'Content-Type':'text/plain'});
		res.end('Hi from our new server!')

	} 

	else if(req.url === "/" && req.method === "POST"){

		res.writeHead(200,{'Content-Type': 'text/plain'});
		res.end('Hi from a new post request with / endpoint.')

	}

	//Mini Activity
	//Create a new route with the following endpoint: /items
	//and with the following method: GET
	//add the proper headers with writeHead()
	//send a simple response: "Items to be shown from this route."

	else if(req.url === "/items" && req.method === "GET"){

		res.writeHead(200,{'Content-Type':'application/json'});
		res.end(JSON.stringify(products));

	}

	//post method route - to add another item into the products array.

	else if(req.url === "/items" && req.method === "POST"){

		//This will act as a placeholder for the body of POST request.
		let requestBody = "";

		//data step - this part will read the stream of data from our client and process the incoming data into the requestBody variable.
		req.on('data',function(data){

			/*console.log(data);*/

			requestBody += data;

		})

		//response ending step - this will run once or after the request has been completely sent from our client.
		req.on('end', function(){

			//requestBody from our postman is JSON to process we need to turn it back to a proper JS object using JSON.parse.
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			//simulate the creation of a new document
			let newItem = {

				name: requestBody.name,
				price: requestBody.price,
				stocks: requestBody.stocks

			}

			//We'll add our newItem into the products array:
			products.push(newItem);
			console.log(products);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.end(JSON.stringify(newItem));


		})

	}

	//put method - edit an item from our array
	else if(req.url === "/items" && req.method === "PUT"){

		let requestBody = "";

		req.on('data',function(data){

			requestBody += data;

		})

		req.on('end',function(){

			console.log(requestBody);
			requestBody = JSON.parse(requestBody);

			//select the proper item from our using the index input from our requestBody:
			products[requestBody.index] = {

				name: requestBody.name,
				price: requestBody.price,
				stocks: requestBody.stocks

			}
			console.log(products[requestBody.index]);

			res.writeHead(200,{"Content-Type": "application/json"});
			res.end(JSON.stringify(products[requestBody.index]));

		})



	}

	//delete method - delete an item from the array

	/*
		Create a new route for the /items endpoint but method should be delete.

		This route should be able to delete the last item in our array.

		As a response, send a message "Item deleted." if the item was deleted.

		To check from your client

		Create a new request same url and endpoint http://localhost:4000/items
		Method: Delete

		Run the request for GET Item to check if the item was deleted.

		Take a screenshot of the response from GET Item with the last item deleted.

		Send the screenshot to the hangouts.

	*/

	else if(req.url === "/items" && req.method === "DELETE"){

		products.pop();
		res.writeHead(200,{"Content-Type": "application/json"});
		res.end("Item Deleted.");

	}

	else if(req.url === "/items/getSingle" && req.method === "POST"){
		/*
			Create a new route with the endpoint: /items/getSingle

			//POST - Pass the name of the item you want to find from your client.
					 
					 -receive the data from the client.

					 -use find() to look for the item in our products array by the providing the proper return condition.

					 -save the value returned by find() into a variable called foundItem.

					 -if foundItem is not undefined, return/send foundItem to our client.

					 -Else foundItem is undefined, send a message to the client: "Item not found."
		*/

		let requestBody = "";

		req.on('data',function(data){

			requestBody += data;

		});


		req.on('end',function(){

			console.log(requestBody);

			requestBody = JSON.parse(requestBody);

			let foundItem = products.find((product)=>{

				return product.name === requestBody.name

			})

			console.log(foundItem);

			if(foundItem !== undefined){
				res.writeHead(200,{'Content-Type': 'application/json'});
				res.end(JSON.stringify(foundItem));
			} else {

				res.writeHead(404,{'Content-Type': 'application/json'});
				res.end("Item not found.")

			}

		})


	}


	else {

		res.writeHead(404,{'Content-Type': 'text/plain'});
		res.end('Resource Not Found!')

	}

}).listen(4000);

console.log(`Server is running at port 4000`)
